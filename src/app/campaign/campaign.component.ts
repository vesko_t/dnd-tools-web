import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {CampaignService} from "./campaign.service";
import {Campaign} from "../model/campaign";
/**
 * Created by vesko on 7.7.2021 г..
 */
@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {

  campaign: Campaign;

  constructor(public route: ActivatedRoute, public campaignService: CampaignService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.campaignService.getCampaign(params['id']).subscribe(res => {
        this.campaign = res;
      })
    })
  }
}
