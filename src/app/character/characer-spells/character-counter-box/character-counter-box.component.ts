import {Component, Input, Output, EventEmitter} from "@angular/core";
import {Counter} from "../../../model/counter";
import {MatDialog} from "@angular/material/dialog";
import {CharacterService} from "../../character.service";
/**
 * Created by vesko on 17.6.2021 г..
 */
@Component({
  selector: 'app-character-counter-box',
  templateUrl: './character-counter-box.component.html',
  styleUrls: ['./character-counter-box.component.scss', '../../character.component.scss']
})
export class CharacterCounterBoxComponent {

  @Input()
  counter: Counter;

  @Output()
  onCounterDelete = new EventEmitter<Counter>();

  counterDialogRef;

  constructor(private dialog: MatDialog, private characterService: CharacterService) {
  }

  onSubtract() {
    this.counter.current -= 1;
    this.updateCounter();
  }

  onAdd() {
    this.counter.current += 1;
    this.updateCounter();
  }

  onReset() {
    this.counter.current = this.counter.max;
    this.updateCounter();
  }

  openCounterDialog(counterDialog) {
    this.counterDialogRef = this.dialog.open(counterDialog);
  }

  saveCounter() {
    this.counter.current = this.counter.max;
    this.updateCounter();
    this.closeDialog();
  }

  deleteCounter() {
    this.onCounterDelete.emit(this.counter);
    this.closeDialog();
  }

  closeDialog() {
    if (typeof this.counterDialogRef != 'undefined') {
      this.counterDialogRef.close();
    }
  }

  updateCounter() {
    this.characterService.updateCounter(this.counter).subscribe(res => {
      this.counter = res;
    })
  }

}
