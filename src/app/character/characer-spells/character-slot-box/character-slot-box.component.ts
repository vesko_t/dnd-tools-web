import {Component, Input, EventEmitter, Output} from "@angular/core";
import {SpellSlot} from "../../../model/spell-slot";
import {MatDialog} from "@angular/material/dialog";
import {CharacterService} from "../../character.service";
/**
 * Created by vesko on 23.5.2021 г..
 */
@Component({
  selector: 'app-character-slot-box',
  templateUrl: './character-slot-box.component.html',
  styleUrls: ['./character-slot-box.component.scss', '../../character.component.scss']
})
export class CharacterSlotBoxComponent {

  @Input()
  spellSlot: SpellSlot;

  @Output()
  onSlotDelete = new EventEmitter<SpellSlot>();

  slotDialogRef;

  constructor(private dialog: MatDialog, private characterService: CharacterService) {
  }

  onSubtract() {
    this.spellSlot.available -= 1;
    this.updateSlot();
  }

  onAdd() {
    this.spellSlot.available += 1;
    this.updateSlot();
  }

  onReset() {
    this.spellSlot.available = this.spellSlot.max;
    this.updateSlot();
  }

  openSlotDialog(slotDialog) {
    this.slotDialogRef = this.dialog.open(slotDialog);
  }

  saveSlot() {
    this.spellSlot.available = this.spellSlot.max;
    this.updateSlot();
    this.closeDialog();
  }

  deleteSlot() {
    this.onSlotDelete.emit(this.spellSlot);
    this.closeDialog();
  }

  closeDialog() {
    if (typeof this.slotDialogRef != 'undefined') {
      this.slotDialogRef.close();
    }
  }

  updateSlot() {
    this.characterService.updateSlot(this.spellSlot).subscribe(res => {
      this.spellSlot = res;
    })
  }

}
