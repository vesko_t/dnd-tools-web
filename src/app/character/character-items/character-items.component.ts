import {Component, Input, Output, EventEmitter, OnInit, ViewChild} from "@angular/core";
import {Character} from "../../model/character";
import {Item} from "../../model/item";
import {style, state, animate, transition, trigger} from "@angular/animations";
import {MatDialog} from "@angular/material/dialog";
import {Weapon} from "../../model/weapon";
import {Dice} from "../../model/dice";
import {DiceType} from "../../model/dice-type";
import {CharacterService} from "../character.service";
import {MatTable} from "@angular/material/table";
/**
 * Created by vesko on 24.5.2021 г..
 */
@Component({
  selector: 'app-character-items',
  templateUrl: './character-items.component.html',
  styleUrls: ['./character-items.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class CharacterItemsComponent implements OnInit {
  @Input()
  character: Character;

  expandedItem: Item | null;
  columns = ['name', 'count', 'edit', 'delete'];

  itemDialogRef;

  itemToEdit: any;

  itemType: string = 'generic';

  diceTypes: Array<any>;

  @ViewChild(MatTable) table: MatTable<any>;


  constructor(private dialog: MatDialog, private characterService: CharacterService) {
  }

  ngOnInit(): void {
    this.diceTypes = [];
    for (let type in DiceType) {
      if (typeof DiceType[type] === 'string') {
        this.diceTypes.push(DiceType[type]);
      }
    }
  }

  getDialogTitle(): string {
    if (this.itemToEdit != null) {
      return typeof this.itemToEdit.id != 'undefined' ? 'Edit Item' : 'New Item'
    }
    return '';
  }

  closeDialog() {
    this.itemToEdit = null;
    this.itemType = 'generic';
    if (typeof this.itemDialogRef != 'undefined') {
      this.itemDialogRef.close();
    }
  }

  editItem(item, itemDialog) {
    if (item != null) {
      this.itemToEdit = item;
      this.itemType = this.itemToEdit.type;
    } else {
      this.itemToEdit = new Item();
      this.itemToEdit.type = 'generic';
    }
    this.itemDialogRef = this.dialog.open(itemDialog);
  }

  saveItem() {
    if (typeof this.itemToEdit.id == 'undefined') {
      this.characterService.addItem(this.character.id, this.itemToEdit).subscribe(res => {
        this.character.items.push(res);
        this.table.renderRows();
      });
    } else {
      this.characterService.updateItem(this.itemToEdit).subscribe(res => {
        for (let item of this.character.items) {
          if (item.id === res.id) {
            item = res;
          }
        }
        if (this.character.currentWeapon.id === res.id) {
          this.character.currentWeapon = res;
        }
        this.table.renderRows();
      });
    }
    this.itemToEdit = null;
    this.itemType = 'generic';
    this.closeDialog();
  }

  onTypeChange() {
    if (this.itemType === 'weapon') {
      this.itemToEdit = new Weapon();
      this.itemToEdit.damage = new Dice();
      this.itemToEdit.damage.count = 1;
      this.itemToEdit.type = 'weapon';
    } else if (this.itemType === 'generic') {
      this.itemToEdit = new Item();
      this.itemToEdit.type = 'generic';
    }
  }

  deleteItem(item) {
    this.characterService.deleteItem(item, this.character).subscribe(res => {
      this.character.items.splice(this.character.items.indexOf(item), 1);
      this.table.renderRows();
    });
  }

  canSave() {
    return this.itemToEdit.name != null && typeof this.itemToEdit.name != 'undefined' && this.itemToEdit.name != '';
  }

}
