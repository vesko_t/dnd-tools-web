/**
 * Created by vesko on 1.7.2021 г..
 */
import {OnInit, Component, Input, ViewChild} from "@angular/core";
import {animate, transition, style, state, trigger} from "@angular/animations";
import {Character} from "../../model/character";
import {Trait} from "../../model/trait";
import {MatTable} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {CharacterService} from "../character.service";
import {Proficiency} from "../../model/proficiency";
/**
 * Created by vesko on 1.7.2021 г..
 */
@Component({
  selector: 'app-character-profs',
  templateUrl: './character-profs.component.html',
  styleUrls: ['./character-profs.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class CharacterProfsComponent implements OnInit {
  @Input()
  character: Character;

  expandedProf: Trait | null;
  columns = ['name', 'edit'];

  profDialogRef;

  @ViewChild(MatTable) table: MatTable<any>;

  profName: string = '';

  profsToSelect: Array<Proficiency>

  constructor(private dialog: MatDialog, private characterService: CharacterService) {
  }


  ngOnInit(): void {
  }

  canSearch() {
    return typeof this.profName != 'undefined' && this.profName != null;
  }

  searchProfs() {
    this.characterService.findProfsByName(this.profName).subscribe(res => {
      this.profsToSelect = res;
    })
  }

  editProfs(profDialog) {
    this.profDialogRef = this.dialog.open(profDialog);
  }

  canAdd(prof) {
    if (typeof this.character.proficienciesObjs != 'undefined' && this.character.proficienciesObjs.length != 0) {
      for (let p of this.character.proficienciesObjs) {
        if (p.id === prof.id) {
          return false;
        }
      }
    }
    return true;
  }

  closeDialog() {
    if (typeof this.profDialogRef != 'undefined') {
      this.profDialogRef.close();
    }
    this.profsToSelect = [];
  }

  deleteProf(prof){
    this.characterService.deleteProf(prof, this.character).subscribe(res => {
      this.character.proficienciesObjs.splice(this.character.proficienciesObjs.indexOf(prof), 1);
      this.table.renderRows();
    });  }

  addProf(prof) {
    this.characterService.addProf(this.character.id, prof).subscribe(res => {
      this.character.proficienciesObjs.push(res);
      this.table.renderRows();
    })
  }
}
