/**
 * Created by vesko on 23.5.2021 г..
 */
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {StatType} from "../../model/stat-type";
import {Character} from "../../model/character";
import {SavingThrow} from "../../model/saving-throw";
import {Weapon} from "../../model/weapon";
import {CharacterService} from "../character.service";
import {Stat} from "../../model/stat";

@Component({
  selector: 'app-character-stats',
  templateUrl: './character-stats.component.html',
  styleUrls: ['./character-stats.component.scss']
})
export class CharacterStatsComponent {

  @Input()
  character: Character;

  @Output()
  onSave: EventEmitter<any> = new EventEmitter<any>();

  constructor(private characterService: CharacterService) {

  }

  onHpKey(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.onSave.emit();
    }
  }

  getStatBonus(savingThrow: SavingThrow): number {
    for (let stat of this.character.stats) {
      if (stat.type === savingThrow.type) {
        return savingThrow.hasProficiency === true ? stat.bonus + this.character.proficiencyBonus : stat.bonus;
      }
    }
    return 0;
  }

  getStatShort(stat: StatType): string {
    return StatType[stat].substring(0, 3);
  }

  getWeapons(): Array<Weapon> {
    let weapons: Array<Weapon> = [];
    for (let item of this.character.items) {
      if (item.type === 'weapon') {
        weapons.push(item as Weapon);
      }
    }
    return weapons;
  }

  compareWeapons(o1: any, o2: any): boolean {
    return o1.id === o2.id;
  }

  saveCharacter() {
    this.onSave.emit();
  }

  getWeaponDamage() {
    if (typeof this.character.currentWeapon != 'undefined' && this.character.currentWeapon != null) {
      let weapon: Weapon = this.character.currentWeapon;
      return weapon.damage.count + weapon.damage.type + ' ' + weapon.damageType + ' Damage';
    }
  }

  getWeaponDesc() {
    if (typeof this.character.currentWeapon != 'undefined' && this.character.currentWeapon != null) {
      return this.character.currentWeapon.description;
    }
  }

  saveThrow(saveTh: SavingThrow) {
    this.characterService.updateThrow(saveTh).subscribe(res => {
      saveTh = res;
    });
  }

  saveWeapon(event) {
    this.characterService.setWeapon(event, this.character.id).subscribe(res => {
      this.character.currentWeapon = res;
    })
  }

  onStatKeyDown(event: KeyboardEvent, stat) {
    if (event.keyCode === 13) {
      this.saveStat(stat);
    }
  }

  saveStat(stat: Stat) {
    this.characterService.updateStat(stat).subscribe(res => {
      for (let s of this.character.stats) {
        if (s.id === res.id) {
          s.bonus = res.bonus;
          s.value = res.value;
        }
      }
    });
  }
}
