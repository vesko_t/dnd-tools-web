import {OnInit, Component, Input, ViewChild} from "@angular/core";
import {animate, transition, style, state, trigger} from "@angular/animations";
import {Character} from "../../model/character";
import {Trait} from "../../model/trait";
import {MatTable} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {CharacterService} from "../character.service";
/**
 * Created by vesko on 1.7.2021 г..
 */
@Component({
  selector: 'app-character-traits',
  templateUrl: './character-traits.component.html',
  styleUrls: ['./character-traits.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class CharacterTraitsComponent implements OnInit {
  @Input()
  character: Character;

  expandedTrait: Trait | null;
  columns = ['name', 'edit'];

  traitDialogRef;

  @ViewChild(MatTable) table: MatTable<any>;

  traitName: string = '';

  traitsToSelect: Array<Trait>

  constructor(private dialog: MatDialog, private characterService: CharacterService) {
  }


  ngOnInit(): void {
  }

  canSearch() {
    return typeof this.traitName != 'undefined' && this.traitName != null;
  }

  searchTraits() {
    this.characterService.findTraitsByName(this.traitName).subscribe(res => {
      this.traitsToSelect = res;
    })
  }

  editTraits(traitDialog) {
    this.traitDialogRef = this.dialog.open(traitDialog);
  }

  canAdd(trait) {
    if (typeof this.character.traits != 'undefined' && this.character.traits.length != 0) {
      for (let t of this.character.traits) {
        if (t.id === trait.id) {
          return false;
        }
      }
    }
    return true;
  }

  closeDialog() {
    if (typeof this.traitDialogRef != 'undefined') {
      this.traitDialogRef.close();
    }
    this.traitsToSelect = [];
  }

  deleteTrait(trait){
    this.characterService.deleteTrait(trait, this.character).subscribe(res => {
      this.character.traits.splice(this.character.traits.indexOf(trait), 1);
      this.table.renderRows();
    });  }

  addTrait(trait) {
    this.characterService.addTrait(this.character.id, trait).subscribe(res => {
      this.character.traits.push(res);
      this.table.renderRows();
    })
  }
}
