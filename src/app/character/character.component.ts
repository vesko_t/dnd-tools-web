import {Component, OnInit, ViewChild, Input, HostListener} from '@angular/core';
import {CharacterService} from "./character.service";
import {ActivatedRoute} from "@angular/router";
import {Character} from "../model/character";
import {StatType} from "../model/stat-type";
import {SpellSlot} from "../model/spell-slot";
import {Stat} from "../model/stat";
import {CharacterSkillsComponent} from "./character-skills/character-skills.component";
import {Spell} from "../model/spell";
import {Item} from "../model/item";
import {Alignment} from "../model/alignment";

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  @Input()
  character: Character;

  addExp: number;

  goldToAdd: number;
  silverToAdd: number;

  @ViewChild(CharacterSkillsComponent)
  characterSkills: CharacterSkillsComponent;

  mobile = false;

  alignments: Array<string> = [];

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.screen.width <= 1000) { // 768px portrait
      this.mobile = true;
    } else {
      this.mobile = false;
    }
    console.log(window.screen.width);
    console.log(this.mobile);
  }

  constructor(public characterService: CharacterService, public route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.characterService.getCharacter(params['id']).subscribe((res: Character) => {
        res.spellSlots.sort((n1: SpellSlot, n2: SpellSlot) => {
          if (n1.lvl > n2.lvl) {
            return 1;
          }

          if (n1.lvl < n2.lvl) {
            return -1;
          }

          return 0;
        });
        res.spells.sort((n1: Spell, n2: Spell) => {
          return n1.name.localeCompare(n2.name);
        });
        res.spells.sort((n1: Spell, n2: Spell) => {
          if (n1.spellSlot > n2.spellSlot) {
            return 1;
          }

          if (n1.spellSlot < n2.spellSlot) {
            return -1;
          }

          return 0;
        });
        res.items.sort((a: Item, b: Item) => {
          return a.name.localeCompare(b.name);
        });

        for (let alignment in Alignment) {
          if (typeof Alignment[alignment] != 'number') {
            this.alignments.push(Alignment[alignment]);
          }
        }

        this.character = res;
      })
    });
    if (window.screen.width <= 1000) { // 768px portrait
      this.mobile = true;
    } else {
      this.mobile = false;
    }
  }

  /*
   * TODO fix input width
   * */
  saveCharacter(event?) {
    let character = new Character();
    character.id = this.character.id;
    character.armorClass = this.character.armorClass;
    character.gold = this.character.gold;
    character.silver = this.character.silver;
    character.maxHp = this.character.maxHp;
    character.currentHp = this.character.currentHp;
    character.notes = this.character.notes;
    character.proficiencies = this.character.proficiencies;
    character.experiencePoints = this.character.experiencePoints;
    character.inspired = this.character.inspired;
    character.speed = this.character.speed;
    character.alignment = this.character.alignment;

    this.characterService.saveCharacter(character).subscribe((res: Character)=> {
      console.log(res);
      this.character.level = res.level;
      this.character.proficiencyBonus = res.proficiencyBonus;
    });
  }

  onNotes(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.saveCharacter();
    }
  }

  onHpKey(event: KeyboardEvent) {
    if (event.keyCode === 13) {
      this.saveCharacter();
    }
  }

  addXp(event?: KeyboardEvent) {
    if (this.addExp != null) {
      if (typeof event != 'undefined') {
        if (event.keyCode === 13) {
          this.character.experiencePoints = parseInt(this.character.experiencePoints.toString()) + parseInt(this.addExp.toString());
          this.addExp = null;
          this.saveCharacter();
        }
      } else {
        this.character.experiencePoints = parseInt(this.character.experiencePoints.toString()) + parseInt(this.addExp.toString());
        this.addExp = null;
        this.saveCharacter();
      }
    }
  }

  addGold(event?: KeyboardEvent) {
    if (this.goldToAdd != null) {
      if (typeof event != 'undefined') {
        if (event.keyCode === 13) {
          this.character.gold = parseInt(this.character.gold.toString()) + parseInt(this.goldToAdd.toString());
          this.goldToAdd = null;
          this.saveCharacter();
        }
      } else {
        this.character.gold = parseInt(this.character.gold.toString()) + parseInt(this.goldToAdd.toString());
        this.goldToAdd = null;
        this.saveCharacter();
      }
    }
  }

  subtractGold() {
    this.character.gold = parseInt(this.character.gold.toString()) - parseInt(this.goldToAdd.toString());
    this.goldToAdd = null;
    this.saveCharacter();
  }

  addSilver(event?: KeyboardEvent) {
    if (this.silverToAdd != null) {
      if (typeof event != 'undefined') {
        if (event.keyCode === 13) {
          this.character.silver = parseInt(this.character.silver.toString()) + parseInt(this.silverToAdd.toString());
          this.silverToAdd = null;
          this.saveCharacter();
        }
      } else {
        this.character.silver = parseInt(this.character.silver.toString()) + parseInt(this.silverToAdd.toString());
        this.silverToAdd = null;
        this.saveCharacter();
      }
    }
  }

  subtractSilver() {
    this.character.silver = parseInt(this.character.silver.toString()) - parseInt(this.silverToAdd.toString());
    this.silverToAdd = null;
    this.saveCharacter();
  }

}
