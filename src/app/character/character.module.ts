import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {CharacterComponent} from "./character.component";
import {RouterModule} from "@angular/router";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {CharacterService} from "./character.service";
import {MatTabsModule} from "@angular/material/tabs";
import {CharacterStatsComponent} from "./character-stats/character-stats.component";
import {CharacterSpellsComponent} from "./characer-spells/character-spells.component";
import {MatTableModule} from "@angular/material/table";
import {CharacterSlotBoxComponent} from "./characer-spells/character-slot-box/character-slot-box.component";
import {MatIconModule} from "@angular/material/icon";
import {MatRippleModule} from "@angular/material/core";
import {CharacterItemsComponent} from "./character-items/character-items.component";
import {CharacterSkillsComponent} from "./character-skills/character-skills.component";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatSelectModule} from "@angular/material/select";
import {MatDialogModule} from "@angular/material/dialog";
import {CharacterCounterBoxComponent} from "./characer-spells/character-counter-box/character-counter-box.component";
import {MatExpansionModule} from "@angular/material/expansion";
import {CharacterTraitsComponent} from "./character-traits/character-traits.component";
import {CharacterProfsComponent} from "./character-profs/character-profs.component";

const routes = [
  {
    path: 'character/:id',
    component: CharacterComponent,
  },
]

@NgModule({
  declarations: [
    CharacterComponent,
    CharacterStatsComponent,
    CharacterSpellsComponent,
    CharacterSlotBoxComponent,
    CharacterItemsComponent,
    CharacterSkillsComponent,
    CharacterCounterBoxComponent,
    CharacterTraitsComponent,
    CharacterProfsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forChild(routes),
    FormsModule,
    HttpClientModule,

    MatTooltipModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    MatTabsModule,
    FlexLayoutModule,
    MatTableModule,
    MatIconModule,
    MatRippleModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    MatExpansionModule
  ],
  providers: [CharacterService]
})
export class CharacterModule {
}
