import {OnInit, Component, ViewChild} from "@angular/core";
import {DmService} from "./dm.service";
import {Character} from "../model/character";
import {MatTableDataSource, MatTable} from "@angular/material/table";
import {animate, transition, style, state, trigger} from "@angular/animations";
import {StatType} from "../model/stat-type";
import {Skill} from "../model/skill";
import {Campaign} from "../model/campaign";
import {MatDialog} from "@angular/material/dialog";
/**
 * Created by vesko on 28.5.2021 г..
 */
@Component({
  selector: 'app-dm',
  templateUrl: './dm.component.html',
  styleUrls: ['./dm.component.scss'],
})
export class DmComponent implements OnInit {

  campaigns: Array<Campaign>;
  columns = ['name', 'add'];

  newCampaign: Campaign;

  campaignDialogRef;

  @ViewChild(MatTable) table: MatTable<any>;


  constructor(public dmService: DmService, private dialog: MatDialog) {
  }


  ngOnInit(): void {
    this.dmService.getAllCampaigns().subscribe((res: Array<Campaign>) => {
      console.log(res);
      this.campaigns = res;
    });
  }

  addCampaign(dialog) {
    this.newCampaign = new Campaign();
    this.campaignDialogRef = this.dialog.open(dialog);
  }

  deleteCampaign(campaign) {

  }

  onSave() {
    this.dmService.saveCampaign(this.newCampaign).subscribe(res => {
      this.campaigns.push(res);
      this.table.renderRows();
    })
  }

  closeDialog() {
    if (typeof this.campaignDialogRef != 'undefined') {
      this.campaignDialogRef.close();
    }
    this.newCampaign = null;
  }
}
