import {DmComponent} from "./dm.component";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {MatTabsModule} from "@angular/material/tabs";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatRippleModule} from "@angular/material/core";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatSelectModule} from "@angular/material/select";
import {MatDialogModule} from "@angular/material/dialog";
import {CharacterService} from "../character/character.service";
import {NgModule} from "@angular/core";
import {CharacterModule} from "../character/character.module";
import {DmService} from "./dm.service";
/**
 * Created by vesko on 28.5.2021 г..
 */
const routes = [
  {
    path: 'dm',
    component: DmComponent,
  },
];

@NgModule({
  declarations: [
    DmComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forChild(routes),
    FormsModule,
    HttpClientModule,

    MatTooltipModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    MatTabsModule,
    FlexLayoutModule,
    MatTableModule,
    MatIconModule,
    MatRippleModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
  ],
  providers: [DmService]
})
export class DmModule {
}
