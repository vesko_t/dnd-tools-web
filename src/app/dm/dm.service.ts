import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Character} from "../model/character";
import {Campaign} from "../model/campaign";

@Injectable({
  providedIn: 'root',
})
export class DmService {

  constructor(private http: HttpClient) {
  }

  public getAllCampaigns(): Observable<Array<Campaign>> {
    let url = '/api/campaigns';

    return this.http.get<Array<Campaign>>(url);
  }

  public saveCampaign(campaign: Campaign): Observable<Campaign> {
    let url = '/api/campaigns';

    return this.http.post<Campaign>(url, campaign);
  }

}
