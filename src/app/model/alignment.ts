/**
 * Created by vesko on 22.5.2021 г..
 */
export enum Alignment{
  LG, NG, CH, LN, TN, CN, LE, NE, CE
}
