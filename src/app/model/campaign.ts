import {Character} from "./character";
import {Npc} from "./npc";
export class Campaign {

  id: number;
  name: string;
  characters: Array<Character>;
  npcs: Array<Npc>;
  items: Array<Npc>;
}
