export class Counter {
  id: string;
  name: string;
  current: number;
  max: number;
}
