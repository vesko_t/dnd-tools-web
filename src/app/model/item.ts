/**
 * Created by vesko on 22.5.2021 г..
 */
export class Item {
  id: number;
  name: string;
  description: string;
  count: number;
}
