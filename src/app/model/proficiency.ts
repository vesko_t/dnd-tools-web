export class Proficiency {
  id: number;

  name: string;

  description: string;
}
