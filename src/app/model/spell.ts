export class Spell {
  id: number;
  name: string;
  description: string;
  spellSlot: number;
  duration: string;
  components: string;
  castTime: string;
  range: string;
  classes: string;
}
