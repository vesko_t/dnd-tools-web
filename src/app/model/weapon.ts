import {Item} from "./item";
import {Dice} from "./dice";
export class Weapon extends Item {

  damage: Dice;
  damageType: string;

}
